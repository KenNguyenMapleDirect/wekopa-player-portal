import React, {useState, useRef, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ActivityIndicator,
  Platform,
  Linking,
} from 'react-native';
import WebView from 'react-native-webview';
import SplashScreen from 'react-native-splash-screen';

const ProfileScreen = ({navigation}) => {
  const [currentUrl, setCurrentUrl] = useState('');
  const webviewRef = useRef(null);

  useEffect(() => {
    SplashScreen.hide();
  }, []);

  let jsCodeAndroid = `  
    const meta = document.createElement('meta');
    meta.setAttribute('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0');
    meta.setAttribute('name', 'viewport');
    document.head.appendChild(meta);
     $('a[target="_blank"]').removeAttr('target');
     $("a[href='https://wekopacasinoresort.mymapleonline.com']").attr('href', 'https://wekopacasinoresort.mymapleonline.com')
    //replace video by image
    document.getElementsByClassName("hero-home")[0].innerHTML = '<img loading="lazy" src="https://s3.us-east-2.amazonaws.com/emails.maplewebservices.com/TestPublic/coverVideo.jpg"  width="100%">'
    //enable button view all
    document.querySelector('.featured-promotions-detail').style.width = '100vw';
    document.querySelector('.featured-promotions-detail').querySelector('a').style.display = 'block'
    document.querySelector('.featured-promotions-detail').querySelector('a').style.width = '60vw';
    `;
  let jsCodeIos = `  
  window.onload = function() {
  //prevent open in new tab
  Array.from(document.querySelectorAll('a[target="_blank"]'))
  .forEach(link => link.removeAttribute('target'));
  //change url login
   let loginHref = document.querySelectorAll("a[href='https://wekopacasinoresort.mymapleonline.com']");
   if(loginHref.length > 0)
   loginHref[0].setAttribute("href", "https://wekopacasinoresort.mymapleonline.com/");
   //replace video by image
   document.getElementsByClassName("hero-home")[0].innerHTML = '<img loading="lazy" src="https://s3.us-east-2.amazonaws.com/emails.maplewebservices.com/TestPublic/coverVideo.jpg" width="100%" height="100%">'
    //enable button view all
    document.querySelector('.featured-promotions-detail').style.width = '100vw';
    document.querySelector('.featured-promotions-detail').querySelector('a').style.display = 'block'
    document.querySelector('.featured-promotions-detail').querySelector('a').style.width = '60vw';
};`;

  return (
    <SafeAreaView style={{flex: 1}}>
      <WebView
        mediaPlaybackRequiresUserAction={true}
        injectedJavaScriptBeforeContentLoaded={
          Platform.OS === 'ios' ? jsCodeIos : jsCodeAndroid
        }
        originWhitelist={['*']}
        injectedJavaScript={Platform.OS === 'ios' ? jsCodeIos : jsCodeAndroid}
        javaScriptEnabledAndroid={true}
        scalesPageToFit={false}
        useWebKit={false}
        source={{uri: 'https://wekopacasinoresort.mymapleonline.com/'}}
        startInLoadingState={true}
        renderLoading={() => (
          <ActivityIndicator
            color="black"
            size="large"
            style={styles.spinLoading}
          />
        )}
        ref={webviewRef}
        onShouldStartLoadWithRequest={(navState) => {
          // console.log(navState.url);
          if (navState.url === 'mailto:fortuneclub@wekopacasinoresort.com') {
            Linking.openURL(navState.url).catch((er) => {
              // console.log('Failed to open Link: ' + er.message);
            });
            return false;
          } else {
            setCurrentUrl(navState.url);
            return true;
          }
        }}
        onNavigationStateChange={(navState) => {
          if (navState.url === 'mailto:fortuneclub@wekopacasinoresort.com') {
            Linking.openURL(navState.url).catch((er) => {
              // console.log('Failed to open Link: ' + er.message);
            });
            return false;
          } else {
            setCurrentUrl(navState.url);
            return true;
          }
        }}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    width: 300,
    marginTop: 16,
  },
});
export default ProfileScreen;
