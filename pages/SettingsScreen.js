import * as React from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  Text,
  SafeAreaView,
} from 'react-native';
import ToggleSwitch from 'rn-toggle-switch'

const SettingsScreen = ({route, navigation}) => {
  return (
    <SafeAreaView style={{flex: 1,    flexDirection: 'row',    alignItems: 'center',}}>
      <View style={{flex: 1, padding: 16,    alignItems: 'center',}}>
        <Text style={styles.baseText}>Nortification Options</Text>
        <View style={styles.switchWrapper}>
        <ToggleSwitch
            text={{on: 'ON', off: 'OFF', activeTextColor: 'white', inactiveTextColor: '#B7B8BA'}}
            textStyle={{fontWeight: 'bold'}}
            color={{ indicator: 'white', active: 'rgba(32, 193, 173, 1)', inactive:  'rgba( 247, 247, 247, 1)', activeBorder: '#41B4A4', inactiveBorder: '#E9E9E9'}}
            active={true}
            disabled={false}
            width={60}
            height={10}
            radius={25}
            onValueChange={(val) => {
              /* your handler function... */
            }}
        />
        </View>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  baseText: {
    alignItems: 'center',
    flexGrow: 0,
    fontSize: 18,
    color: 'white'
  },
  switchWrapper: {
    flexGrow: 1,
    padding: 3
  }
});
export default SettingsScreen;
