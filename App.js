import React, {useState, useRef, useEffect} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    StatusBar,
    View,
    Button,
    Dimensions,
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import 'react-native-gesture-handler';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import GetLocation from 'react-native-get-location';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import PushNotification from 'react-native-push-notification';
import { WebView } from 'react-native-webview';

import HomeScreen from './pages/HomeScreen';
import ProfileScreen from './pages/ProfileScreen';
import SettingsScreen from './pages/SettingsScreen';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function HomeStack() {
    return (
        <Stack.Navigator
            initialRouteName="Home"
            screenOptions={{
                headerStyle: {backgroundColor: '#000000'},
                headerTintColor: '#fff',
                headerTitleStyle: {fontWeight: 'bold'},
            }}>
            <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={{title: 'Home Page'}}
            />
            <Stack.Screen
                name="Profile"
                component={ProfileScreen}
                options={{title: 'Profile Page'}}
            />
            <Stack.Screen
                name="Settings"
                component={SettingsScreen}
                options={{title: 'Settings Page'}}
            />
        </Stack.Navigator>
    );
}

function SettingsStack() {
    return (
        <Stack.Navigator
            initialRouteName="Settings"
            screenOptions={{
                headerStyle: {backgroundColor: '#000000'},
                headerTintColor: '#fff',
                headerTitleStyle: {fontWeight: 'bold'},
            }}>
            <Stack.Screen
                name="Settings"
                component={SettingsScreen}
                options={{title: 'Setting Page'}}
            />
            <Stack.Screen
                name="Profile"
                component={ProfileScreen}
                options={{title: 'Profile Page'}}
            />
        </Stack.Navigator>
    );
}

function ProfileStack() {
    return (
        <Stack.Navigator
            initialRouteName="Profile"
            screenOptions={{
                headerStyle: {backgroundColor: '#000000'},
                headerTintColor: '#fff',
                headerTitleStyle: {fontWeight: 'bold'},
            }}>
            <Stack.Screen
                name="Settings"
                component={SettingsScreen}
                options={{title: 'Setting Page'}}
            />
            <Stack.Screen
                name="Profile"
                component={ProfileScreen}
                options={{title: 'Profile Page'}}
            />
        </Stack.Navigator>
    );
}

export const notificationInit = () => {
    PushNotification.createChannel(
        {
            channelId: 'wekopa-news', // (required)
            channelName: 'WeKoPa News', // (required)
            channelDescription: 'A channel to display wekopa offers', // (optional) default: undefined.
            playSound: true, // (optional) default: true
            soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
            importance: 4, // (optional) default: 4. Int value of the Android notification importance
            vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
        },
        (created) => console.log(`createChannel returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
    );
    // Must be outside of any component LifeCycle (such as `componentDidMount`).
    PushNotification.configure({
        // (optional) Called when Token is generated (iOS and Android)
        onRegister: function (token) {
            console.log('TOKEN:', token);
        },

        // (required) Called when a remote is received or opened, or local notification is opened
        onNotification: function (notification) {
            console.log('onNotification:', notification);
            if (notification.foreground) {
                PushNotification.localNotification({
                    title: notification.title,
                    message: notification.message,
                });
            }
            // process the notification

            // (required) Called when a remote is received or opened, or local notification is opened
            notification.finish(PushNotificationIOS.FetchResult.NoData);
        },

        // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
        onAction: function (notification) {
            console.log('onAction:', notification.action);
            // console.log('NOTIFICATION:', notification);
            if (notification.action === 'Yes') {
                PushNotification.invokeApp(notification);
                console.log('click yes!');
                //get location to choose which type of context will display
                GetLocation.getCurrentPosition({
                    enableHighAccuracy: true,
                    timeout: 15000,
                })
                    .then(location => {
                        console.log('Your Location Data:', location);
                        let distance = calcCrow(location.latitude, location.longitude);
                        if (distance <= 25) {
                            // LocalNotification();
                        } else {
                            console.log('distance (km):', distance);
                        }
                    })
                    .catch(error => {
                        const {code, message} = error;
                        console.warn('Get Location Error', code, message);
                    });
            } else {
                console.log('click no!');
                return false;
            }
            // process the action
        },

        // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
        onRegistrationError: function (err) {
            console.error(err.message, err);
        },

        // IOS ONLY (optional): default: all - Permissions to register.
        permissions: {
            alert: true,
            badge: true,
            sound: true,
        },

        // Should the initial notification be popped automatically
        // default: true
        popInitialNotification: true,

        /**
         * (optional) default: true
         * - Specified if permissions (ios) and token (android and ios) will requested or not,
         * - if not, you must call PushNotificationsHandler.requestPermissions() later
         * - if you are not using remote notification or do not have Firebase installed, use this:
         *     requestPermissions: Platform.OS === 'ios'
         */
        requestPermissions: Platform.OS === 'ios',
    });
};

const LocalNotificationSchedule = () => {
    let today = new Date();
    today.setHours(16);
    today.setMinutes(11);
    today.setSeconds(50);
    today.setMilliseconds(0);
    console.log('schedule');
    PushNotification.localNotificationSchedule({
        channelId: 'wekopa-news',
        autoCancel: true,
        bigText:
            'You have New Offer from WeKoPa Resort. Open to view details.',
        subText: 'now',
        title: 'New Offer',
        message: 'Open to view details',
        vibrate: true,
        vibration: 300,
        playSound: true,
        soundName: 'default',
        allowWhileIdle: true,
        actions: '["Yes", "No"]',
        date: today, // in 60 secs
        repeatType: 'day',
        invokeApp: false,
    });

};
notificationInit();
LocalNotificationSchedule();

//This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
const calcCrow = (lat1, lon1) => {
    //define lat2, lon2 as Wekopa resort
    var lat2 = 33.5809722;
    var lon2 = -111.6817036;
    var R = 6371; // km
    var dLat = toRad(lat2 - lat1);
    var dLon = toRad(lon2 - lon1);
    lat1 = toRad(lat1);
    lat2 = toRad(lat2);

    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
};

// Converts numeric degrees to radians
const toRad = (Value) => {
    return Value * Math.PI / 180;
};

const App = () => {
    const [dt, setDt] = useState(new Date().getHours());

    useEffect(() => {
        SplashScreen.hide();
    }, []);

    const MyTheme = {
        dark: false,
        colors: {
            primary: 'rgb(255, 45, 85)',
            background: '#000',
            card: 'rgb(255, 255, 255)',
            text: 'rgb(28, 28, 30)',
            border: 'rgb(199, 199, 204)',
            notification: 'rgb(255, 69, 58)',
        },
    };

    return (
        <>
            <View style={{marginTop: 20}}>
            </View>
            <StatusBar barStyle="dark-content"/>
            <SafeAreaView style={styles.flexContainer}>
                <NavigationContainer theme={MyTheme}>
                    <Tab.Navigator
                        initialRouteName="Feed"
                        tabBarOptions={{
                            labelStyle: {
                                fontSize: 14,
                            },
                            activeTintColor: 'red',
                            style: {
                                backgroundColor: 'black',
                            },
                        }}>
                        <Tab.Screen
                            name="HomeStack"
                            component={HomeStack}
                            options={{
                                tabBarLabel: 'Home',
                                tabBarIcon: ({color, size}) => (
                                    <MaterialCommunityIcons
                                        name="home-outline"
                                        color="white"
                                        size={size}
                                    />
                                ),
                            }}
                        />
                        <Tab.Screen
                            name="ProfileStack"
                            component={ProfileStack}
                            options={{
                                tabBarLabel: 'Profile',
                                tabBarIcon: ({color, size}) => (
                                    <MaterialCommunityIcons
                                        name="account-outline"
                                        color="white"
                                        size={size}
                                    />
                                ),
                            }}
                        />
                        <Tab.Screen
                            name="SettingsStack"
                            component={SettingsStack}
                            options={{
                                tabBarLabel: 'Settings',
                                tabBarIcon: ({color, size}) => (
                                    <MaterialCommunityIcons
                                        name="settings-helper"
                                        color="white"
                                        size={size}
                                    />
                                ),
                            }}
                        />
                    </Tab.Navigator>
                </NavigationContainer>
            </SafeAreaView>
        </>
    );
};

const styles = StyleSheet.create({
    flexContainer: {
        flex: 1,
    },
    spinLoading: {
        position: 'absolute',
        top: Dimensions.get('window').height / 2,
        left: Dimensions.get('window').width / 2,
        flex: 1,
    },
    tabBarContainer: {
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: 'black',
    },
    button: {
        color: 'white',
        fontSize: 24,
    },
});

export default App;
